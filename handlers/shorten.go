package handlers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/AlphaWong/shorturi/helpers"
)

func (this *HttpRequestHandler) ShortenHandler(w http.ResponseWriter, r *http.Request) {
	defer helpers.Logger.Sync()
	vars := mux.Vars(r)
	shortenId := vars["shortenId"]
	targetURL, err := this.persisterService.Get(shortenId)
	if err != nil {
		helpers.Logger.Warn(
			"Error",
			zap.String("shortenId", shortenId),
			zap.Error(err),
		)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	helpers.Logger.Info(
		"Found",
		zap.String("shortenId", shortenId),
		zap.String("targetURL", targetURL),
	)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusFound)
	htmlOutput := fmt.Sprintf(`<meta http-equiv="refresh" content="0; url=%s" />`, targetURL)
	w.Write([]byte(htmlOutput))
}
