package handlers

import (
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"

	"gitlab.com/AlphaWong/shorturi/helpers"
	"gitlab.com/AlphaWong/shorturi/mocks"
)

func TestMain(m *testing.M) {
	helpers.InitLogger()
	code := m.Run()
	os.Exit(code)
}

func TestNewHttpRequestHandler(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)
}

func TestPingHandler(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/ping", nil)

	h.PingHandler(w, r)
	require.Equal(http.StatusOK, w.Result().StatusCode)
}

func TestShortenHandler(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	p.EXPECT().Get("my_key").Return("my_value", nil)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/my_key", nil)

	route := mux.NewRouter()
	route.HandleFunc("/{shortenId}", h.ShortenHandler)
	route.ServeHTTP(w, r)

	require.Equal(http.StatusFound, w.Result().StatusCode)
	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal("text/html; charset=utf-8", w.Result().Header.Get("Content-Type"))
	require.Equal(`<meta http-equiv="refresh" content="0; url=my_value" />`, string(b))
}

func TestShortenHandlerFailByKeyFound(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	p.EXPECT().Get("my_key").Return("", errors.New("I am error"))

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/my_key", nil)

	route := mux.NewRouter()
	route.HandleFunc("/{shortenId}", h.ShortenHandler)
	route.ServeHTTP(w, r)

	require.Equal(http.StatusInternalServerError, w.Result().StatusCode)
	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal("text/plain; charset=utf-8", w.Result().Header.Get("Content-Type"))
	require.Equal(http.StatusText(http.StatusInternalServerError)+"\n", string(b))
}

func TestNewURLHandler(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	s.EXPECT().GetNewID().Return("my_key")
	p.EXPECT().Set("my_key", "http://google.com").Return(true, nil)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	jsonString := `{"url": "http://google.com"}`
	r := httptest.NewRequest(http.MethodPost, "/newurl", strings.NewReader(jsonString))

	route := mux.NewRouter()
	route.HandleFunc("/newurl", h.NewURLHandler)
	route.ServeHTTP(w, r)

	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal(http.StatusOK, w.Result().StatusCode)
	require.Equal("application/json", w.Result().Header.Get("Content-Type"))
	require.Equal("{\"url\":\"http://google.com\",\"shortenUrl\":\"http:///my_key\"}\n", string(b))
}

func TestNewURLHandlerFailInvalidURL(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	s.EXPECT().GetNewID().Return("my_key").Times(0)
	p.EXPECT().Set("my_key", "://google.com").Return(true, nil).Times(0)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	jsonString := `{"url": "://google.com"}`
	r := httptest.NewRequest(http.MethodPost, "/newurl", strings.NewReader(jsonString))

	route := mux.NewRouter()
	route.HandleFunc("/newurl", h.NewURLHandler)
	route.ServeHTTP(w, r)

	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal(http.StatusBadRequest, w.Result().StatusCode)
	require.Equal("text/plain; charset=utf-8", w.Result().Header.Get("Content-Type"))
	require.Equal("invalid URL ://google.com, error parse \"://google.com\": missing protocol scheme\n", string(b))
}

func TestNewURLHandlerFailMissingHostOrScheme(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	s.EXPECT().GetNewID().Return("my_key").Times(0)
	p.EXPECT().Set("my_key", "google.com").Return(true, nil).Times(0)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	jsonString := `{"url": "google.com"}`
	r := httptest.NewRequest(http.MethodPost, "/newurl", strings.NewReader(jsonString))

	route := mux.NewRouter()
	route.HandleFunc("/newurl", h.NewURLHandler)
	route.ServeHTTP(w, r)

	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal(http.StatusBadRequest, w.Result().StatusCode)
	require.Equal("text/plain; charset=utf-8", w.Result().Header.Get("Content-Type"))
	require.Equal("invalid URL google.com\n", string(b))
}

func TestNewURLHandlerFailKeyDuplicated(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	s.EXPECT().GetNewID().Return("my_key").Times(1)
	p.EXPECT().Set("my_key", "http://google.com").Return(false, nil).Times(1)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	jsonString := `{"url": "http://google.com"}`
	r := httptest.NewRequest(http.MethodPost, "/newurl", strings.NewReader(jsonString))

	route := mux.NewRouter()
	route.HandleFunc("/newurl", h.NewURLHandler)
	route.ServeHTTP(w, r)

	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal(http.StatusInternalServerError, w.Result().StatusCode)
	require.Equal("text/plain; charset=utf-8", w.Result().Header.Get("Content-Type"))
	require.Equal("Internal Server Error\n", string(b))
}
func TestNewURLHandlerFailNetworkIssue(t *testing.T) {
	require := require.New(t)
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()
	s := mocks.NewMockShortener(ctrl)
	p := mocks.NewMockPersister(ctrl)
	s.EXPECT().GetNewID().Return("my_key").Times(1)
	p.EXPECT().Set("my_key", "http://google.com").Return(false, errors.New("time out ~~")).Times(1)

	h := NewHttpRequestHandler(s, p)
	require.NotNil(h)

	w := httptest.NewRecorder()
	jsonString := `{"url": "http://google.com"}`
	r := httptest.NewRequest(http.MethodPost, "/newurl", strings.NewReader(jsonString))

	route := mux.NewRouter()
	route.HandleFunc("/newurl", h.NewURLHandler)
	route.ServeHTTP(w, r)

	b, err := io.ReadAll(w.Result().Body)
	require.NoError(err)
	require.Equal(http.StatusInternalServerError, w.Result().StatusCode)
	require.Equal("text/plain; charset=utf-8", w.Result().Header.Get("Content-Type"))
	require.Equal("time out ~~\n", string(b))
}
