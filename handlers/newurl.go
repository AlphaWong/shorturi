package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"go.uber.org/zap"

	"gitlab.com/AlphaWong/shorturi/helpers"
)

func (this *HttpRequestHandler) NewURLHandler(w http.ResponseWriter, r *http.Request) {
	defer helpers.Logger.Sync()
	var input NewURLRequest
	json.NewDecoder(r.Body).Decode(&input)

	u, err := url.Parse(input.URL)
	if err != nil {
		helpers.Logger.Warn(
			"Invalid URL",
			zap.String("URL", input.URL),
			zap.Error(err),
		)
		http.Error(w, fmt.Sprintf("invalid URL %s, error %s", input.URL, err), http.StatusBadRequest)
		return
	}

	if u.Scheme == "" || u.Host == "" {
		helpers.Logger.Warn(
			"Missing URL scheme or host",
			zap.String("URL", input.URL),
			zap.String("Host", u.Host),
			zap.String("Scheme", u.Scheme),
			zap.Error(err),
		)
		http.Error(w, fmt.Sprintf("invalid URL %s", input.URL), http.StatusBadRequest)
		return
	}

	shortenId := this.shortenerService.GetNewID()
	ok, err := this.persisterService.Set(shortenId, input.URL)
	// if the !ok means duplicated key
	// notthing will be saved
	if !ok && err == nil {
		helpers.Logger.Warn(
			"Cannot save",
			zap.String("key", input.URL),
			zap.String("value", shortenId),
			zap.Bool("ok", ok),
			zap.Error(err),
		)

		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if err != nil {
		helpers.Logger.Warn(
			"Cannot save with error",
			zap.String("key", input.URL),
			zap.String("value", shortenId),
			zap.Bool("ok", ok),
			zap.Error(err),
		)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	helpers.Logger.Info(
		"Saved",
		zap.String("key", input.URL),
		zap.String("value", shortenId),
	)

	shortenURL := fmt.Sprintf("http://%s/%s", os.Getenv("HOST_NAME"), shortenId)
	output := NewURLResponse{
		URL:        input.URL,
		ShortenURL: shortenURL,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(output)
}
