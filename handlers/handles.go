package handlers

import (
	"net/http"
	"sync"

	"gitlab.com/AlphaWong/shorturi/services"
)

type RequestHandler interface {
	NewURLHandler(w http.ResponseWriter, r *http.Request)
	PingHandler(w http.ResponseWriter, r *http.Request)
	ShortenHandler(w http.ResponseWriter, r *http.Request)
}

var _ RequestHandler = (*HttpRequestHandler)(nil)

type HttpRequestHandler struct {
	mu               sync.Mutex
	shortenerService services.Shortener
	persisterService services.Persister
}

func NewHttpRequestHandler(
	s services.Shortener,
	p services.Persister,
) RequestHandler {
	return &HttpRequestHandler{
		shortenerService: s,
		persisterService: p,
	}
}
