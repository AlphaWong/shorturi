package handlers

type NewURLRequest struct {
	URL string `json:"url"`
}

type NewURLResponse struct {
	URL        string `json:"url"`
	ShortenURL string `json:"shortenUrl"`
}
