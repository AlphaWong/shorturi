[![pipeline status](https://gitlab.com/AlphaWong/shorturi/badges/main/pipeline.svg)](https://gitlab.com/AlphaWong/shorturi/-/commits/main)
[![coverage report](https://gitlab.com/AlphaWong/shorturi/badges/main/coverage.svg)](https://alphawong.gitlab.io/shorturi/coverage.html)
[![codecov](https://codecov.io/gl/AlphaWong/shorturi/branch/main/graph/badge.svg?token=MAJY5349Y5)](https://codecov.io/gl/AlphaWong/shorturi)
# shorturi

# doc
https://gitlab.com/AlphaWong/shorturi/-/wikis/home

# skaffold
## quick test
```cmd
minikube start --driver=hyperkit
skaffold run
minikube tunnel
```
## minikube issue
https://minikube.sigs.k8s.io/docs/drivers/docker/#known-issues

## registry
https://gitlab.com/AlphaWong/shorturi/container_registry

# test
```sh
make test
make coverage
```

# requirement
1. brew install skaffold

# postman collection
https://www.getpostman.com/collections/14b91d85f30d4acc102a

# config
.env

# local development
```sh
make run
```

## local redis
```sh
docker run -d -p 6379:6379 redis:6.2.6-alpine
```

## generate mock
```sh
go install github.com/golang/mock/mockgen@latest
mockgen -package=mocks -destination=mocks/mock_services.go gitlab.com/AlphaWong/shorturi/services Shortener,Persister
```

# reference
1. https://github.com/gorilla/mux
1. https://blog.kowalczyk.info/article/JyRZ/generating-good-unique-ids-in-go.html
1. https://github.com/segmentio/ksuid
1. https://stackoverflow.com/a/55531345/2876087

# question ?
wck.alpha@gmail.com