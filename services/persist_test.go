package services

import (
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/stretchr/testify/require"
)

func TestNewRedisPersister(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
}

func TestRedisGetKeySuccess(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	s.Set("my_key", "my_value")
	val, err := client.Get("my_key")
	require.NoError(err)
	require.Equal("my_value", val)
}

func TestRedisGetKeyNotFound(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	s.Set("my_key", "my_value")
	val, err := client.Get("my_key2")
	require.Error(err)
	require.Equal("", val)
}

func TestRedisSetKey(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	ok, err := client.Set("my_key", "my_value")
	require.NoError(err)
	require.True(ok)
}

func TestRedisSetKeyFailedByDuplicateKey(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	s.Set("my_key", "my_value")
	ok, err := client.Set("my_key", "my_value")
	require.NoError(err)
	require.False(ok)
}

func TestRedisCheckKeyExistSuccess(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	s.Set("my_key", "my_value")
	ok := client.Exist("my_key")
	require.True(ok)
}

func TestRedisCheckKeyExistFailByNotFound(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	defer s.Close()
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	s.Set("my_key", "my_value")
	ok := client.Exist("my_key1")
	require.False(ok)
}

func TestRedisCheckKeyExistFailConnectionLost(t *testing.T) {
	require := require.New(t)
	s, err := miniredis.Run()
	require.NoError(err)
	client := NewRedisPersister(
		WithAddress(s.Addr()),
		WithDbName(0),
		WithPassword(""),
		WithUserName(""),
	)
	require.NotNil(client)
	s.Set("my_key", "my_value")
	s.Close()
	ok := client.Exist("my_key1")
	require.False(ok)
}
