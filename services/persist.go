package services

import (
	"context"
	"sync"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

type Persister interface {
	Get(key string) (string, error)
	Set(key string, values string) (bool, error)
	Exist(key string) bool
}

type RedisPersister struct {
	mu     sync.Mutex
	client *redis.Client
}

var _ Persister = (*RedisPersister)(nil)

type RedisPersisterOption func(*redis.Options)

func WithAddress(addr string) RedisPersisterOption {
	return func(option *redis.Options) {
		option.Addr = addr
	}
}

func WithPassword(password string) RedisPersisterOption {
	return func(option *redis.Options) {
		option.Password = password
	}
}

func WithUserName(userName string) RedisPersisterOption {
	return func(option *redis.Options) {
		option.Username = userName
	}
}

func WithDbName(db int) RedisPersisterOption {
	return func(option *redis.Options) {
		option.DB = db
	}
}

func NewRedisPersister(setters ...RedisPersisterOption) Persister {
	defaultOption := &redis.Options{}
	for _, setter := range setters {
		setter(defaultOption)
	}
	return &RedisPersister{
		client: redis.NewClient(defaultOption),
	}
}

func (this *RedisPersister) Get(key string) (string, error) {
	val, err := this.client.Get(ctx, key).Result()
	return val, err
}

func (this *RedisPersister) Set(key, value string) (bool, error) {
	ok, err := this.client.SetNX(ctx, key, value, redis.KeepTTL).Result()
	return ok, err
}

func (this *RedisPersister) Exist(key string) bool {
	_, err := this.client.Get(ctx, key).Result()
	if err == redis.Nil {
		return false
	}
	if err != nil {
		return false
	}
	return true
}
