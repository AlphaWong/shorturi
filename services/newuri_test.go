package services

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewKuidShortener(t *testing.T) {
	require := require.New(t)
	result := NewKuidShortener()
	require.NotNil(result)
}

func TestGetNewID(t *testing.T) {
	require := require.New(t)
	result := NewKuidShortener()
	require.NotEmpty(result.GetNewID())
}
