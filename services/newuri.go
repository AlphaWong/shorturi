package services

import (
	"sync"

	"github.com/segmentio/ksuid"
)

type Shortener interface {
	GetNewID() string
}

var _ Shortener = (*KuidShortener)(nil)

type KuidShortener struct {
	mu sync.Mutex
}

func NewKuidShortener() Shortener {
	return &KuidShortener{}
}

func (*KuidShortener) GetNewID() string {
	return ksuid.New().String()
}
