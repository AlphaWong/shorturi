package helpers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestInitLogger(t *testing.T) {
	require := require.New(t)
	InitLogger()
	require.NotNil(Logger)
}
