package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/AlphaWong/shorturi/handlers"
	"gitlab.com/AlphaWong/shorturi/helpers"
	"gitlab.com/AlphaWong/shorturi/services"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

var port = ":8080"

func main() {
	initConfig()
	handlers := GetTransportionLayerAndConnect2Service()
	runServer(handlers)
}

func initConfig() {
	err := godotenv.Load()
	port = ":" + os.Getenv("PORT")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	helpers.InitLogger()
}

func GetTransportionLayerAndConnect2Service() handlers.RequestHandler {
	k := services.NewKuidShortener()
	s := services.NewRedisPersister(
		services.WithAddress(os.Getenv("REDIS_HOST")),
		services.WithPassword(""),
		services.WithUserName(""),
		services.WithDbName(0),
	)
	return handlers.NewHttpRequestHandler(
		k,
		s,
	)
}

func runServer(h handlers.RequestHandler) {
	r := mux.NewRouter()
	r.HandleFunc("/ping", h.PingHandler).Methods(http.MethodGet)
	r.HandleFunc("/newurl", h.NewURLHandler).Methods(http.MethodPost)
	r.HandleFunc("/{shortenId}", h.ShortenHandler).Methods(http.MethodGet)

	srv := &http.Server{
		Handler: r,
		Addr:    port,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Printf("listen at %s \n", os.Getenv("PORT"))
	log.Fatal(srv.ListenAndServe())
}
