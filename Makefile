run:
	go run ./main.go
test:
	go clean -testcache && go test ./... -v
race:
	go clean -testcache && go test -race ./... -v
cover: 
	go clean -testcache && go test -coverprofile=coverage.out -covermode=atomic ./...
coverage:
	go clean -testcache && go test -tags=coverage ./... -coverprofile coverage.out && go tool cover -func=coverage.out
coveragehtml: coverage
	go tool cover -html=coverage.out -o public/coverage.html